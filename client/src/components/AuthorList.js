import React, { Component } from "react";
import { gql } from "apollo-boost";
import { graphql, Query } from "react-apollo";

const getAuthorsQuery = gql`
  {
    authors {
      name
      age
    }
  }
`;
const AuthorList = () => (
  <Query query={getAuthorsQuery}>
    {({ loading, error, data }) => {
      if (loading) return <p>Loading...</p>;
      if (error) return <p>Error :(</p>;

      return data.authors.map(({ name, age,  id }) => (
        <div key={name}>
          <ul id="author-list">
            <li>
              {name}:{age}
            </li>
          </ul>
        </div>
      ));
    }}
  </Query>
);

export default graphql(getAuthorsQuery)(AuthorList);
