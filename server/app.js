const express = require("express");
const graphqlHTTP = require("express-graphql");
const schema = require("./schema/schema");
const mongoose = require("mongoose");
const cors = require("cors");

const app = express();

//allow cross-origin requests
app.use(cors())

//connect to mlab db
//make sure to replace my db string & creds with your own
mongoose.connect("mongodb://user:test1234@ds245287.mlab.com:45287/gql-grace");
mongoose.connection.once('open',()=>{
    console.log('connected to mongodb');
})
app.use(
  "/graphql",
  graphqlHTTP({
    schema,
    graphiql:true
  })
);

app.listen(4000, () => {
  console.log("now listening requests on port 4000");
});
