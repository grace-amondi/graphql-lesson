import React, { Component } from "react";
import { gql } from "apollo-boost";
import { graphql, Query } from "react-apollo";

const getBooksQuery = gql`
  {
    books {
      name
      genre
      id
      author {
        name
      }
    }
  }
`;

class BookList extends Component {
  componentDidMount() {
    console.log("page mounted");
  }

  bookList(){
    return (
      <Query query={getBooksQuery} notifyOnNetworkStatusChange>
        {({ loading, error, data, refetch, networkStatus }) => {
          if (networkStatus === 4) return "Reloading..!";
          if (loading) return null;
          if (error) return `Error!: ${error}`;

          return data.books.map(({ name, id, genre }) => (
            <div key={id}>
              <ul id="book-list">
                <li>
                  {name}
                </li>
              </ul>
              {/* <button onClick={() => refetch()}>Refresh!</button> */}
            </div>
          ));
        }}
      </Query>
    );
  }

  render() {
    console.log(this.render);
    return(
      <div>
        {this.bookList()}
      </div>
    )
  }

  componentWillUpdate(nextProps, nextState) {
    console.log("page updated");
  }
}

export default graphql(getBooksQuery)(BookList);
